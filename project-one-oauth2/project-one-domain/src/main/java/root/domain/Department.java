package root.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="department")
@Getter
@Setter
@ToString
public class Department implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="number_of_employees")
	private int numberOfEmployees;
	@ManyToMany()
	@JoinTable(name = "company_department", joinColumns=@JoinColumn(name = "company_id"), inverseJoinColumns=@JoinColumn(name = "department_id"))
	private List<Company> companies = new ArrayList<>();
	@OneToMany(mappedBy="department")
	private List<Employee> employees;

	public Department() {
	}

	public Department(int id, String name, int numberOfEmployees) {
		this.id = id;
		this.name = name;
		this.numberOfEmployees = numberOfEmployees;
	}
}
