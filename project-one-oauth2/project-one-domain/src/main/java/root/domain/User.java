package root.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.crypto.bcrypt.BCrypt;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "user")
@Getter
@Setter
@ToString
public class User {

	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
	private Set<Role> roles = new HashSet<>();

	public User() {
	}

	public User(int id, String username, String password) {
		this.id = id;
		this.username = username;
		setPassword(password);
	}

	public void setPassword(String password) {
		this.password = BCrypt.hashpw(password, "Salted");
	}

}
