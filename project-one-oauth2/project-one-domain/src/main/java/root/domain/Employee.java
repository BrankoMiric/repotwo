package root.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "employee")
@Getter
@Setter
@ToString
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "full_name")
	private String fullName;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "email")
	private String email;
	@Column(name = "salary")
	private BigDecimal salary;
	@ManyToOne
	private Department department;

	public Employee() {
	}

	public Employee(String fullName, String phoneNumber, String email, BigDecimal salary) {
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.salary = salary;
	}

}
