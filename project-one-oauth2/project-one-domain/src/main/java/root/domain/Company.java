package root.domain;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "company")
@Getter
@Setter
@ToString
public class Company implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "country")
	private String country;
	@Column(name = "city")
	private String city;
	@Column(name = "address")
	private String address;
	@ManyToMany(mappedBy = "companies")
	private List<Department> departments = new ArrayList<>();

	public Company() {
	}

	public Company(int id, String name, String country, String city, String address) {
		this.id = id;
		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

}
