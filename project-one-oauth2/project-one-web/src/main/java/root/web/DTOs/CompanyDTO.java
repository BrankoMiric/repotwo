package root.web.DTOs;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompanyDTO {
	
	private int id;
	private String name;
	private String country;
	private String city;
	private String address;

}
