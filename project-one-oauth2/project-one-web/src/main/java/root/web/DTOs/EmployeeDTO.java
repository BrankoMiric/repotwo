package root.web.DTOs;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmployeeDTO {

	private int id;
	private String fullName;
	private String phoneNumber;
	private String email;
	private BigDecimal salary;

}
