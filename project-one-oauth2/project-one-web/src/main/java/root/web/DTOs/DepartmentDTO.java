package root.web.DTOs;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DepartmentDTO {

	private int id;
	private String name;
	private int numberOfEmployees;

}
