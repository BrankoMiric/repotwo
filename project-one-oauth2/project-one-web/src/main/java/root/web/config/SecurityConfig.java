package root.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import root.persistence.services.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public UserService userService;
	
//	@Autowired
//    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userService);
//    }
	
	@Autowired
	public void configureAuthentication(AuthenticationManagerBuilder builder) throws Exception {
		builder.inMemoryAuthentication().withUser("User1").password("{bcrypt}$2a$04$wqH9Mvn7j4hGUeRt1GK3D.E0m4AZ2lZ4vWRSLAWy5FLJMvs2As2Py").roles("ADMIN", "USER");
		builder.inMemoryAuthentication().withUser("User2").password("{bcrypt}$2a$04$Q9uh1kIGyNhHxWNlmy8wNOrcej.6GUCKsQwXdVC30P3hNu2zVB.R6").roles("USER");
		builder.inMemoryAuthentication().withUser("User3").password("{bcrypt}$2a$04$umJt1Boyg1JKDDsj8anaeujjytw9iBjSDyKlRFWE1Aic1xB6FBc9q").roles("GUEST");
		
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		 http
         .anonymous().disable()
         .authorizeRequests()
         .antMatchers("/**").permitAll();
		 
		 http.csrf().disable();
		 
		 http.headers().frameOptions().disable();
	}

	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}

}
