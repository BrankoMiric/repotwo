package root.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value= {"persistence.properties", "web.properties"})
public class PropertiesConfig {

}
