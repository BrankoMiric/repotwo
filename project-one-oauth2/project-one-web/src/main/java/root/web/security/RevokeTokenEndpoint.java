package root.web.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FrameworkEndpoint
public class RevokeTokenEndpoint {

	@Autowired
	private ConsumerTokenServices tokenServices;

	@Autowired
	private TokenStore tokenStore;

	@RequestMapping(method = RequestMethod.DELETE, value = "/oauth/token")
	@ResponseBody
	public void revokeToken(HttpServletRequest request) {
		Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientId("client");
		for (OAuth2AccessToken token : tokens) {
			tokenServices.revokeToken(token.getValue());
		}
	}
}
