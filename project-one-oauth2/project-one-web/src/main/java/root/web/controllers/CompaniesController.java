package root.web.controllers;

import static root.web.controllers.CompaniesController.BASE;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import root.domain.Company;
import root.persistence.services.ICompaniesService;
import root.web.DTOs.CompanyDTO;

@RestController
@RequestMapping(value = BASE)
public class CompaniesController {

	protected final static String BASE = "companies/";
	private final static String GET_ALL = "company";
	private final static String GET_BY_CITY = "company/{city}";
	private final static String POST_NEW = "company";
	private final static String PUT_UPDATE = "company";
	private final static String DELETE_ONE = "company/{id}";

	private ICompaniesService companiesService;
	private ModelMapper modelMapper;

	@Autowired
	public CompaniesController(ICompaniesService companiesService, ModelMapper modelMapper) {
		this.companiesService = companiesService;
		this.modelMapper = modelMapper;
	}

	@RequestMapping(value = GET_ALL, method = RequestMethod.GET)
	public List<CompanyDTO> getAll() {
		List<CompanyDTO> dtos = new ArrayList<>();
		for (Company company : companiesService.getAllCompanies()) {
			System.out.println(company.getName());
			CompanyDTO dto = modelMapper.map(company, CompanyDTO.class);
			dtos.add(dto);
		}
		return dtos;
	}

	@RequestMapping(value = GET_BY_CITY, method = RequestMethod.GET)
	public List<CompanyDTO> getCompanyByCity(@PathVariable String city) {
		List<CompanyDTO> dtos = new ArrayList<>();
		for (Company company : companiesService.getCompaniesByCity(city)) {
			CompanyDTO dto = modelMapper.map(company, CompanyDTO.class);
			dtos.add(dto);
		}
		return dtos;
	}

	@RequestMapping(value = POST_NEW, method = RequestMethod.POST)
	public CompanyDTO insertCompany(@RequestBody CompanyDTO companyDTO) {
		Company company = modelMapper.map(companyDTO, Company.class);
		Company newCompany = companiesService.createNewCompany(company);
		return modelMapper.map(newCompany, CompanyDTO.class);
	}

	@RequestMapping(value = PUT_UPDATE, method = RequestMethod.PUT)
	public CompanyDTO updateCompany(@RequestBody CompanyDTO companyDTO) {
		Company company = modelMapper.map(companyDTO, Company.class);
		Company updatedCompany = companiesService.updateCompanyData(company);
		return modelMapper.map(updatedCompany, CompanyDTO.class);
	}

	@RequestMapping(value = DELETE_ONE, method = RequestMethod.DELETE)
	public void deleteCompany(@PathVariable int id) {
		companiesService.deleteCompany(id);
	}

}
