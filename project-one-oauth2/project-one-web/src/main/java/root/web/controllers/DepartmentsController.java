package root.web.controllers;

import static root.web.controllers.DepartmentsController.BASE;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import root.domain.Department;
import root.persistence.services.IDepartmentsService;
import root.web.DTOs.DepartmentDTO;

@RestController
@RequestMapping(value = BASE)
public class DepartmentsController {

	protected final static String BASE = "departments/";
	private final static String GET_ALL = "department";
	private final static String GET_BY_NAME = "department/{name}";
	private final static String POST_NEW = "department";
	private final static String DELETE = "department/{id}";
	private final static String PUT_CHANGE_NAME = "department/{currentName}/{newName}";

	@Autowired
	private IDepartmentsService departmentsService;
	@Autowired
	private ModelMapper modelMapper;

	public DepartmentsController(IDepartmentsService departmentsService, ModelMapper modelMapper) {
		this.departmentsService = departmentsService;
		this.modelMapper = modelMapper;
	}

	@RequestMapping(value = GET_ALL, method = RequestMethod.GET)
	public List<DepartmentDTO> getAll() {
		List<DepartmentDTO> dtos = new ArrayList<>();
		for (Department department : departmentsService.getAllDepartments()) {
			DepartmentDTO dto = modelMapper.map(department, DepartmentDTO.class);
			dtos.add(dto);
		}
		return dtos;
	}

	@RequestMapping(value = GET_BY_NAME, method = RequestMethod.GET)
	public DepartmentDTO getByName(@PathVariable String name) {
		Department department = departmentsService.getDepartmentByName(name);
		return modelMapper.map(department, DepartmentDTO.class);
	}

	@RequestMapping(value = POST_NEW, method = RequestMethod.POST)
	public DepartmentDTO createNew(@RequestBody DepartmentDTO departmentDTO) {
		Department department = modelMapper.map(departmentDTO, Department.class);
		Department newDepartment = departmentsService.createNewDepartment(department);
		return modelMapper.map(newDepartment, DepartmentDTO.class);
	}

	@RequestMapping(value = DELETE, method = RequestMethod.DELETE)
	public void deleteDepartment(@PathVariable int id) {
		departmentsService.deleteDepartment(id);
	}

	@RequestMapping(value = PUT_CHANGE_NAME, method = RequestMethod.PUT)
	public void changeName(@PathVariable String currentName, @PathVariable String newName) {
		departmentsService.changeDepartmentName(currentName, newName);
	}
}
