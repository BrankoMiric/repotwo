package root.web.controllers;

import static root.web.controllers.EmployeesController.BASE;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import root.domain.Employee;
import root.persistence.services.IEmployeesService;
import root.web.DTOs.EmployeeDTO;

@RestController
@RequestMapping(value = BASE)
public class EmployeesController {

	protected final static String BASE = "employees/";
	private final static String GET_ALL = "employee";
	private final static String GET_BY_DEPARTMENT = "employee/{department}";
	private final static String POST_NEW = "employee";
	private final static String DELETE_ONE = "employee/{id}";
	private final static String DELETE_BY_DEPARTMENT = "employee/delete/{department}";
	private final static String PUT_CHANGE_DEPARTMENT = "employee/{id}/{department}";
	private final static String GET_EMPLOYEE_HIGHEST_SALARY = "employee/highestsalary";

	private IEmployeesService employeesService;
	private ModelMapper modelMapper;

	@Autowired
	public EmployeesController(IEmployeesService employeesService, ModelMapper modelMapper) {
		this.employeesService = employeesService;
		this.modelMapper = modelMapper;
	}

	@RequestMapping(value = GET_ALL, method = RequestMethod.GET)
	public List<EmployeeDTO> getAll() {
		List<EmployeeDTO> dtos = new ArrayList<>();
		for (Employee employee : employeesService.getAllEmployees()) {
			EmployeeDTO dto = modelMapper.map(employee, EmployeeDTO.class);
			dtos.add(dto);
		}
		return dtos;
	}

	@RequestMapping(value = GET_BY_DEPARTMENT, method = RequestMethod.GET)
	public Long getTotalByDepartment(@PathVariable String department) {
		return employeesService.getTotalNumberOfEmployeesByDepartment(department);
	}

	@RequestMapping(value = POST_NEW, method = RequestMethod.POST)
	public EmployeeDTO createNew(@RequestBody EmployeeDTO employeeDTO) {
		Employee employee = modelMapper.map(employeeDTO, Employee.class);
		Employee newEmployee = employeesService.createEmployee(employee);
		return modelMapper.map(newEmployee, EmployeeDTO.class);
	}

	@RequestMapping(value = DELETE_ONE, method = RequestMethod.DELETE)
	public void deleteEmploye(@PathVariable int id) {
		employeesService.deleteEmploye(id);
	}

	@RequestMapping(value = DELETE_BY_DEPARTMENT, method = RequestMethod.DELETE)
	public void deleteByDepartment(@PathVariable String department) {
		employeesService.deleteEmployeesByDepartment(department);
	}

	@RequestMapping(value = PUT_CHANGE_DEPARTMENT, method = RequestMethod.PUT)
	public void changeDepartment(@PathVariable int id, @PathVariable String department) {
		employeesService.changeDepartmentNameForEmployee(id, department);
	}

	@RequestMapping(value = GET_EMPLOYEE_HIGHEST_SALARY, method = RequestMethod.GET)
	public EmployeeDTO getEmployeeWithHighestSalary() {
		Employee employee = employeesService.getEmployeeWithHighestSalary();
		return modelMapper.map(employee, EmployeeDTO.class);
	}
}
