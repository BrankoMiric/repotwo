package root.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import root.domain.Employee;
import root.persistence.services.IEmployeesService;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeesControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private EmployeesController employeesController;

    @Mock
    private IEmployeesService employeesService;

    @Mock
    private ModelMapper modelMapper;

    @Before
    public void setUp() throws Exception {
        employeesController = new EmployeesController(employeesService, modelMapper);
        mockMvc = MockMvcBuilders.standaloneSetup(employeesController).build();
    }

    @Test
    public void testGetAll() throws Exception{
        mockMvc.perform(get("/employees/employee").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetTotalByDepartment() throws Exception{
        mockMvc.perform(get("/employees/employee/{department}", "IT").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testCreateNew() throws Exception{
        Employee jeff = new Employee("Jeff Bezos", "000-111-333", "jeff@amazon.com", new BigDecimal(10000));
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(jeff);
        mockMvc.perform(post("/employees/employee").contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testDeleteEmployee() throws Exception{
        mockMvc.perform(delete("/employees/employee/{id}", 3)).andExpect(status().isOk());
    }

    @Test
    public void testDeleteByDepartment() throws Exception{
        mockMvc.perform(delete("/employees/employee/delete/{department}", "IT")).andExpect(status().isOk());
    }

    @Test
    public void testChangeDepartment() throws Exception{
        mockMvc.perform(put("/employees/employee/{id}/{department}", 2, "Human Resources")).andExpect(status().isOk());
    }

    @Test
    public void testGetEmployeeWithHighestSalary() throws Exception{
        mockMvc.perform(get("/employees/employee/highestsalary").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}