package root.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import root.domain.Company;
import root.persistence.services.ICompaniesService;

@RunWith(SpringJUnit4ClassRunner.class)
public class CompaniesControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private CompaniesController companiesController;

    @Mock
    private ICompaniesService companiesService;

    @Mock
    private ModelMapper modelMapper;

    @org.junit.Before
    public void setUp() throws Exception {
        companiesController = new CompaniesController(companiesService, modelMapper);
        mockMvc = MockMvcBuilders.standaloneSetup(companiesController).build();
    }

    @org.junit.Test
    public void testGetAll() throws Exception {
        mockMvc.perform(get("/companies/company").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @org.junit.Test
    public void testGetCompanyByCity() throws Exception{
        mockMvc.perform(get("/companies/company/{city}", "Seattle").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @org.junit.Test
    public void testInsertCompany() throws Exception{
        Company apple = new Company(2, "Apple Inc", "USA", "Cupertino", "Apple Park, 1 Apple Park Way");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(apple);
        mockMvc.perform(post("/companies/company").contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @org.junit.Test
    public void testUpdateCompany() throws Exception{
        Company apple = new Company(2, "Apple Inc", "USA", "Cupertino", "Apple Park, 1 Apple Park Way");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(apple);
        mockMvc.perform(put("/companies/company/").contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @org.junit.Test
    public void testDeleteCompany() throws Exception{
        mockMvc.perform(delete("/companies/company/{id}", 5)).andExpect(status().isOk());
    }
}