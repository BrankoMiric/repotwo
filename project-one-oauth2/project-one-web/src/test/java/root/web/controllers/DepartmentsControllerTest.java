package root.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import root.domain.Department;
import root.persistence.services.IDepartmentsService;

@RunWith(SpringJUnit4ClassRunner.class)
public class DepartmentsControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private DepartmentsController departmentsController;

    @Mock
    private IDepartmentsService departmentsService;

    @Mock
    private ModelMapper modelMapper;

    @Before
    public void setUp() throws Exception {
        departmentsController = new DepartmentsController(departmentsService, modelMapper);
        mockMvc = MockMvcBuilders.standaloneSetup(departmentsController).build();
    }

    @Test
    public void testGetAll() throws Exception{
        mockMvc.perform(get("/departments/department").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetByName() throws Exception{
        mockMvc.perform(get("/departments/department/{name}", "IT").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testCreateNew() throws Exception{
        Department hr = new Department(3, "HR", 100);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(hr);
        mockMvc.perform(post("/departments/department").contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testDeleteDepartment() throws Exception{
        mockMvc.perform(delete("/departments/department/{id}", 3)).andExpect(status().isOk());
    }

    @Test
    public void testChangeName() throws Exception{
        mockMvc.perform(put("/departments/department/{currentName}/{newName}", "HR", "Human Resources")).andExpect(status().isOk());
    }
}