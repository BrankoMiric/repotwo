package root.persistence.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;

@Configuration
@TestPropertySource("h2.properties")
@EntityScan("root.domain") 
@EnableJpaRepositories(basePackages="root.persistence.repositories")
public class RepositoryIntegrationTestConfig {

}
