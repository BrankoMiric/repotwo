package root.persistence.services;

import java.util.List;

import root.domain.Employee;

public interface IEmployeesService {

	List<Employee> getAllEmployees();

	Long getTotalNumberOfEmployeesByDepartment(String department);

	Employee createEmployee(Employee employee);

	void deleteEmploye(int id);

	void deleteEmployeesByDepartment(String department);

	void changeDepartmentNameForEmployee(int id, String newDepartment);

	Employee getEmployeeWithHighestSalary();

}