package root.persistence.services;

import java.util.List;

import root.domain.Department;
import root.persistence.repositories.DepartmentsRepository;

public interface IDepartmentsService {

	void setDepartmentsRepository(DepartmentsRepository departmentsRepository);

	List<Department> getAllDepartments();

	Department getDepartmentByName(String name);

	Department createNewDepartment(Department department);

	void deleteDepartment(int id);

	void changeDepartmentName(String currentName, String newName);

}