package root.persistence.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.domain.Department;
import root.persistence.repositories.DepartmentsRepository;

@Service
public class DepartmentsService implements IDepartmentsService {

	@Autowired
	private DepartmentsRepository departmentsRepository;

	@Override
	public void setDepartmentsRepository(DepartmentsRepository departmentsRepository) {
		this.departmentsRepository = departmentsRepository;
	}

	@Override
	public List<Department> getAllDepartments() {
		return (List<Department>) departmentsRepository.findAll();
	}

	@Override
	public Department getDepartmentByName(String name) {
		return departmentsRepository.findByName(name);
	}

	@Override
	public Department createNewDepartment(Department department) {
		return departmentsRepository.save(department);
	}

	@Override
	public void deleteDepartment(int id) {
		departmentsRepository.deleteById(id);
	}

	@Override
	public void changeDepartmentName(String currentName, String newName) {
		departmentsRepository.changeDepartmentName(currentName, newName);
	}
}
