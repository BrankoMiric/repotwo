package root.persistence.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import root.domain.Employee;
import root.persistence.repositories.EmployeesRepository;

@Service
public class EmployeesService implements IEmployeesService {

	@Autowired
	private EmployeesRepository employeesRepository;

	public EmployeesService(EmployeesRepository employeesRepository) {
		this.employeesRepository = employeesRepository;
	}

	@Override
	public List<Employee> getAllEmployees() {
		return (List<Employee>) employeesRepository.findAll();
	}

	@Override
	public Long getTotalNumberOfEmployeesByDepartment(String department) {
		return employeesRepository.countByDepartmentName(department);
	}

	@Override
	public Employee createEmployee(Employee employee) {
		return employeesRepository.save(employee);
	}

	@Override
	public void deleteEmploye(int id) {
		employeesRepository.deleteById(id);
	}

	@Override
	@Transactional
	public void deleteEmployeesByDepartment(String department) {
		employeesRepository.deleteByDepartmentName(department);
	}

	@Override
	public void changeDepartmentNameForEmployee(int id, String newDepartment) {
		employeesRepository.changeDepartmentNameForEmployee(id, newDepartment);
	}

	@Override
	public Employee getEmployeeWithHighestSalary() {
		return employeesRepository.findByMaxSalary();
	}
}
