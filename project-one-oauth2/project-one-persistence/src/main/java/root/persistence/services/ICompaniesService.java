package root.persistence.services;

import java.util.List;

import root.domain.Company;

public interface ICompaniesService {

	List<Company> getAllCompanies();

	List<Company> getCompaniesByCity(String city);

	Company createNewCompany(Company company);

	Company updateCompanyData(Company company);

	void deleteCompany(int id);

}