package root.persistence.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.domain.Company;
import root.persistence.repositories.CompaniesRepository;

@Service
public class CompaniesService implements ICompaniesService {

	@Autowired
	private CompaniesRepository companiesRepository;

	public CompaniesService(CompaniesRepository companiesRepository) {
		this.companiesRepository = companiesRepository;
	}

	@Override
	public List<Company> getAllCompanies() {
		return (List<Company>) companiesRepository.findAll();
	}

	@Override
	public List<Company> getCompaniesByCity(String city) {
		return companiesRepository.findByCity(city);
	}

	@Override
	public Company createNewCompany(Company company) {
		return companiesRepository.save(company);
	}

	@Override
	public Company updateCompanyData(Company company) {
		return companiesRepository.save(company);
	}

	@Override
	public void deleteCompany(int id) {
		companiesRepository.deleteById(id);
	}

}
