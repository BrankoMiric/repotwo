package root.persistence.services;

import root.domain.User;

public interface IUserService {

	User getUserByUsername(String username);

}