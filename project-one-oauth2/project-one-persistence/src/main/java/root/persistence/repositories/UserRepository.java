package root.persistence.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import root.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

	User findByUsername(String username);
}
