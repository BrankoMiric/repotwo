package root.persistence.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import root.domain.Company;

@Repository
public interface CompaniesRepository extends CrudRepository<Company, Integer>{
	
	List<Company> findByCity(String city);

}
