package root.persistence.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import root.domain.Employee;

@Repository
public interface EmployeesRepository extends CrudRepository<Employee, Integer>{

	void deleteByDepartmentName(String department);
	
	Long countByDepartmentName(String department);

	@Transactional
	@Query(value = "select * from employee order by salary desc limit 1", nativeQuery = true)
	Employee findByMaxSalary();
	
	@Transactional
	@Modifying
	@Query(value = "update employee set department_id = (select id from department where name = :name) where id = :id", nativeQuery = true)
	void changeDepartmentNameForEmployee(@Param("id") int id, @Param("name") String name);
}
