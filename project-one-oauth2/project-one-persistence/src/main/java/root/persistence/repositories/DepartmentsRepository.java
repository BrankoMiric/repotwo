package root.persistence.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import root.domain.Department;

@Repository
public interface DepartmentsRepository extends CrudRepository<Department, Integer>{

	Department findByName(String name);

    @Transactional
    @Modifying
	@Query("update Department set name=:newName where name=:currentName")
    void changeDepartmentName(@Param("currentName") String currentName, @Param("newName") String newName);
}
