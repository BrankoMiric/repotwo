create table company(id int primary key auto_increment, name nvarchar(100), country nvarchar(100), city nvarchar(100), address nvarchar(100));
create table department(id int primary key auto_increment, name nvarchar(100), number_of_employees int unsigned);
create table company_department(id int primary key auto_increment, company_id int, department_id int, foreign key(company_id) references company(id), foreign key(department_id) references department(id));
create table employee(id int primary key auto_increment, full_name nvarchar(100), phone_number varchar(20), email nvarchar(50), salary decimal, department_id int, foreign key(department_id) references department(id));




