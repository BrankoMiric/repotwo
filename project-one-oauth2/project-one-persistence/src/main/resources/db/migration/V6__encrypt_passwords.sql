update user set password = "{bcrypt}$2a$04$wqH9Mvn7j4hGUeRt1GK3D.E0m4AZ2lZ4vWRSLAWy5FLJMvs2As2Py" where id = 1;
update user set password = "{bcrypt}$2a$04$Q9uh1kIGyNhHxWNlmy8wNOrcej.6GUCKsQwXdVC30P3hNu2zVB.R6" where id = 2;
update user set password = "{bcrypt}$2a$04$umJt1Boyg1JKDDsj8anaeujjytw9iBjSDyKlRFWE1Aic1xB6FBc9q" where id = 3;