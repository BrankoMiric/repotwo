insert into company(name, country, city, address) values('Apple Inc', 'USA', 'Cupertino', 'Apple Park, 1 Apple Park Way'),
('Amazon', 'USA', 'Seattle', 'South Lake Union');
insert into department(name, number_of_employees) values('IT', 100), ('HR', 200), ('Accounting', 150);
insert into company_department(company_id, department_id) values(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3);
insert into employee(full_name, phone_number, email, salary, department_id) values('Jeff Bezos', '555-333', 'jeff@amazon.com', 1000000.00, 1),
('Steve Wozniak', '555-333', 'steve@apple.com', 900000.00, 1),
('Perla Haney-Jardine', '555-333', 'phj@apple.com', 800000.00, 2),
('Richard Pryor', '555-333', 'richie@gmail.com', 70000.00, 2);

insert into user(username, password) values('User1', 'letmein'), ('User2', 'opensesame'), ('User3', 'unlock');
insert into role(user_role) values('admin'), ('user'), ('guest');
insert into user_role(user_id, role_id) values(1, 1), (1, 2), (2, 2), (3, 3);




