create table user(id int primary key auto_increment, username nvarchar(100), password nvarchar(200));
create table role(id int primary key auto_increment, user_role varchar(20));
create table user_role(id int primary key auto_increment, user_id int, role_id int, foreign key(user_id) references user(id), foreign key(role_id) references role(id));