package root.persistence.repositories;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import root.domain.Department;
import root.persistence.config.RepositoryIntegrationTestConfig;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = RepositoryIntegrationTestConfig.class)
public class DepartmentsRepositoryIT {

	@Autowired
	private DepartmentsRepository departmentsRepository;

	@Test
	public void testFindDepartmentById() {
		Optional<Department> department = departmentsRepository.findById(1);
		assertEquals(department.get().getName(), "IT");
	}

}
