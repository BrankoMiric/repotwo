package root.persistence.repositories;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import root.persistence.config.RepositoryIntegrationTestConfig;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = RepositoryIntegrationTestConfig.class)
public class EmployeesRepositoryIT {

	@Autowired
	private EmployeesRepository employeesRepository;

	@Test
	public void testCountByDepartmentName() {
		Long count = employeesRepository.countByDepartmentName("IT");
		assertEquals(count.toString(), "2");
	}

}
