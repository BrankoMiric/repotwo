package root.persistence.repositories;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import root.domain.Company;
import root.persistence.config.RepositoryIntegrationTestConfig;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = RepositoryIntegrationTestConfig.class)
public class CompaniesRepositoryIT {

	@Autowired
	private CompaniesRepository companiesRepository;

	@Test
	public void testFindByCity() {
		List<Company> companies = companiesRepository.findByCity("Seattle");
		assertEquals("Amazon", companies.get(0).getName());
	}

}
