package root.persistence.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import root.domain.Company;
import root.persistence.repositories.CompaniesRepository;

public class CompaniesServiceTest {

    private CompaniesService companiesService;

    @Mock
    private CompaniesRepository companiesRepository;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        companiesService = new CompaniesService(companiesRepository);
    }

    @Test
    public void testGetCompanyList() {
        List<Company> data = new ArrayList<Company>();
        Company ms = new Company(1, "Microsoft Corporation", "USA", "Redmond", "Microsoft Redmond Campus");
        data.add(ms);
        when(companiesService.getAllCompanies()).thenReturn(data);
        List<Company> companies = companiesService.getAllCompanies();
        assertEquals(companies.size(), data.size());
        verify(companiesRepository, times(1)).findAll();
    }

    @Test
    public void testGetCompanyListByCity() {
        List<Company> data = new ArrayList<Company>();
        Company ms = new Company(1, "Microsoft Corporation", "USA", "Redmond", "Microsoft Redmond Campus");
        data.add(ms);
        String city = "Redmond";
        when(companiesService.getCompaniesByCity(city)).thenReturn(data);
        List<Company> companies = companiesService.getCompaniesByCity(city);
        assertEquals(companies.size(), data.size());
        verify(companiesRepository, times(1)).findByCity(city);
    }

    @Test
    public void testAddNewCompany() {
        Company apple = new Company(2, "Apple Inc", "USA", "Cupertino", "Apple Park, 1 Apple Park Way");
        Company createdCompany = companiesService.createNewCompany(apple);
        when(companiesService.createNewCompany(apple)).thenReturn(apple);
        assertEquals(2, apple.getId());
        assertEquals("Apple Inc", apple.getName());
        verify(companiesRepository, times(1)).save(apple);
    }

    @Test
    public void testUpdateCompany(){
        Company apple = new Company(2, "Apple Inc", "USA", "Cupertino", "Apple Park, 1 Apple Park Way");
        Company updatedCompany = companiesService.updateCompanyData(apple);
        when(companiesService.updateCompanyData(apple)).thenReturn(apple);
        assertEquals(2, apple.getId());
        assertEquals("Apple Inc", apple.getName());
        verify(companiesRepository, times(1)).save(apple);
    }

    @Test
    public void testDeleteCompanyById(){
        companiesService.deleteCompany(1);
        verify(companiesRepository, times(1)).deleteById(1);
    }
}
