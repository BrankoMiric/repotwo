package root.persistence.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import root.domain.Employee;
import root.persistence.repositories.EmployeesRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmployeesServiceTest {

    private EmployeesService employeesService;

    @Mock
    private EmployeesRepository employeesRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        employeesService = new EmployeesService(employeesRepository);
    }

    @Test
    public void testGetAllEmployees() {
        List<Employee> data = new ArrayList<>();
        Employee jeff = new Employee();
        jeff.setFullName("Jeff Bezos");
        data.add(jeff);
        when(employeesService.getAllEmployees()).thenReturn(data);
        List<Employee> employees = employeesService.getAllEmployees();
        assertEquals(1, employees.size());
        verify(employeesRepository, times(1)).findAll();

    }

    @Test
    public void testGetTotalNumberOfEmployeesByDepartment() {
        Long total = 2L;
        when(employeesService.getTotalNumberOfEmployeesByDepartment("IT")).thenReturn(total);
        Long result = employeesService.getTotalNumberOfEmployeesByDepartment("IT");
        assertEquals(total, result);
        verify(employeesRepository, times(1)).countByDepartmentName("IT");
    }

    @Test
    public void testCreateEmployee() {
        Employee jeff = new Employee();
        jeff.setFullName("Jeff Bezos");
        when(employeesService.createEmployee(jeff)).thenReturn(jeff);
        Employee employee = employeesService.createEmployee(jeff);
        assertEquals(jeff.getFullName(), employee.getFullName());
        verify(employeesRepository, times(1)).save(jeff);
    }

    @Test
    public void testDeleteEmployee() {
        employeesService.deleteEmploye(1);
        verify(employeesRepository, times(1)).deleteById(1);
    }

    @Test
    public void testDeleteEmployeesByDepartment() {
        employeesService.deleteEmployeesByDepartment("IT");
        verify(employeesRepository, times(1)).deleteByDepartmentName("IT");
    }

    @Test
    public void testChangeDepartmentNameForEmployee() {
        employeesService.changeDepartmentNameForEmployee(1, "HR");
        verify(employeesRepository, times(1)).changeDepartmentNameForEmployee(1, "HR");
    }

    @Test
    public void testGetEmployeeWithHighestSalary() {
        Employee jeff = new Employee();
        jeff.setFullName("Jeff Bezos");
        when(employeesService.getEmployeeWithHighestSalary()).thenReturn(jeff);
        Employee employee = employeesService.getEmployeeWithHighestSalary();
        assertEquals(employee.getFullName(), jeff.getFullName());
        verify(employeesRepository, times(1)).findByMaxSalary();
    }
}