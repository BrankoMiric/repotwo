package root.persistence.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import root.domain.Department;
import root.persistence.repositories.DepartmentsRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DepartmentsServiceTest {

    private DepartmentsService departmentsService;

    @Mock
    private DepartmentsRepository departmentsRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        departmentsService = new DepartmentsService();
        departmentsService.setDepartmentsRepository(departmentsRepository);
    }

    @Test
    public void testGetAllDepartments() {
        List<Department> data = new ArrayList<>();
        Department it = new Department();
        it.setName("IT");
        data.add(it);
        when(departmentsService.getAllDepartments()).thenReturn(data);
        List<Department> departments = departmentsService.getAllDepartments();
        assertEquals(departments.size(), data.size());
        verify(departmentsRepository, times(1)).findAll();
    }

    @Test
    public void testGetDepartmentByName() {
        Department it = new Department();
        it.setName("IT");
        String name = "IT";
        when(departmentsService.getDepartmentByName(name)).thenReturn(it);
        Department department = departmentsService.getDepartmentByName(name);
        assertEquals(it.getName(), it.getName());
        verify(departmentsRepository, times(1)).findByName(name);
    }

    @Test
    public void testCreateNewDepartment() {
        Department it = new Department();
        it.setName("IT");
        Department department = departmentsService.createNewDepartment(it);
        when(departmentsService.createNewDepartment(it)).thenReturn(it);
        assertEquals(it.getName(), it.getName());
        verify(departmentsRepository, times(1)).save(it);
    }

    @Test
    public void testDeleteDepartment() {
        departmentsService.deleteDepartment(1);
        verify(departmentsRepository, times(1)).deleteById(1);
    }

    @Test
    public void testChangeDepartmentName() {
        Department it = new Department();
        it.setName("IT");
        departmentsService.changeDepartmentName("IT", "HR");
        verify(departmentsRepository, times(1)).changeDepartmentName("IT", "HR");
    }
}