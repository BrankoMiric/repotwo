-- drop schema project1;
-- create schema project1;
-- use project1;
-- create table company(id int primary key auto_increment, name nvarchar(100), country nvarchar(100), city nvarchar(100), address nvarchar(100));
-- create table department(id int primary key auto_increment, name nvarchar(100), number_of_employees int unsigned);
-- create table company_department(id int primary key auto_increment, company_id int, department_id int, foreign key(company_id) references company(id), foreign key(department_id) references department(id));
-- create table employee(id int primary key auto_increment, full_name nvarchar(100), phone_number varchar(20), email nvarchar(50), salary decimal, department_id int, foreign key(department_id) references department(id));
-- insert into company(name, country, city, address) values("Apple Inc", "USA", "Cupertino", "Apple Park, 1 Apple Park Way"),
-- ("Amazon", "USA", "Seattle", "South Lake Union");
-- insert into department(name, number_of_employees) values("IT", 100), ("HR", 200), ("Accounting", 150);
-- insert into company_department(company_id, department_id) values(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3);
-- insert into employee(full_name, phone_number, email, salary, department_id) values("Jeff Bezos", "555-333", "jeff@amazon.com", 1000000.00, 1),
-- ("Steve Wozniak", "555-333", "steve@apple.com", 900000.00, 1),
-- ("Perla Haney-Jardine", "555-333", "phj@apple.com", 800000.00, 2),
-- ("Richard Pryor", "555-333", "richie@gmail.com", 70000.00, 2);
-- 
select * from employee;
select * from department;



